#include<stdio.h>
#include<math.h>
struct point
{
    float x;
    float y;
};
typedef struct point Point;
Point input()
{
    Point p;
    printf("enter absicca\n");
    scanf("%f",&p.x);
    printf("enter ordinate\n");
    scanf("%f",&p.y);
    return p;
}
float compute(Point p1,Point p2)
{
    float distance;
    distance=sqrt((p2.x-p1.x)*(p2.x-p1.x)+(p2.y-p1.y)*(p2.y-p1.y));
    return distance;
}
void output(Point p1,Point p2,float distance)
{
    printf("the distance between %f,%f and %f,%f is %f\n",p1.x,p2.x,p1.y,p2.y,distance);
}
int main(void)
{
    float distance;
    Point p1,p2;
    p1=input();
    p2=input();
    distance=compute(p1,p2);
    output(p1,p2,distance);
    return 0;
}
